FROM docker:18.09.0

ENV PATH="/opt/gitlab-ci/bin:${PATH}"
RUN echo "export PATH=$PATH" > /etc/environment
ADD bin/* /opt/gitlab-ci/bin/

CMD ["/opt/gitlab-ci/bin/docker-help"]
