# CI Base

Example project to show off ability for CI to test its self.  See [presenation](https://drubin.github.io/presentations/2018/ci-cd-meetup-berlin-accelerate-pipeline/#1) for full over view.


## Example usage

Add the following to your project's `.gitlab-ci.yml`
```yaml
stages:
  - setup
  - test
  - release

include:
  - https://gitlab.com/drubin/ci-base/raw/master/includes/docker.yml
  - https://gitlab.com/drubin/ci-base/raw/master/includes/node.yml

image: node:10.13-alpine
```

![Node pipeline](images/node-master-pipeline.png)


### How this project works
This project uses its self to build and test its self.

So if you were to make a breaking change

```diff
docker pull "$CI_REGISTRY_IMAGE:latest" || true
- docker build --cache-from "$CI_REGISTRY_IMAGE:latest" --tag "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA" .
+ docker build --cache-from "$CI_REGISTRY_IMAGE:latest" --tag "$CI_REGISTRY_IMAGE:broken" .
```

![ci-base](images/ci-base-broken-pr.png)


### Functions


#### docker-build
Builds the docker image with distributed caching using correct naming.

```bash
docker-build
```


#### workflow-master
Releases the latest docker image with the commit `:git-hash` and `:latest`

```bash
workflow-master
```
